@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <h3>Datos del postulante {{$postulante->getNombre()}}</h3>
      <hr>
    </div>
  </div>
    <div class="row">
        <div class="col-md-8">
          <h4>DNI: {{$postulante->dni}}</h4>
          <h4>Email: {{$postulante->email}}</h4>
          <h4>Fecha de nacimiento: {{$postulante->fecha_nac}}</h4>
          <h4>Domicilio: {{$postulante->domicilio}}</h4>
          <h4>Teléfono: {{$postulante->telefono}}</h4>
          <h4>Fecha de expedición de título: {{$postulante->fecha_titulo}}</h4>
          <h4>Universidad otorgante: {{$postulante->universidad_otorgante}}</h4>
          
          <h4>Antecedentes</h4>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                            <td>Descripción</td>
                            <td>Institucion</td>
                            <td>Fecha de Emisión</td>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($postulante->antecedentes as $antecedente)
                        <div class="row">
                          <td>{{$antecedente->descripcion}}</td>
                          <td>{{$antecedente->institucion_otorgante}}</td>
                          <td>{{$antecedente->fecha_emision}}</td>                    
                      @endforeach          
                      </tbody>
                    </table>
        </div>
      </div>
</div>
@endsection
