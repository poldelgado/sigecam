@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-header">
                  <h3>Perfil de postulante {{ $postulante->getNombre() }}</h3>
                </div>

                <div class="panel-body">
                  <h4>DNI: {{ $postulante->dni }}</h4>
                  <h4>Email: {{ $postulante->email }}</h4>
                  <h4>Fecha de nacimiento: {{ $postulante->fecha_nac }}</h4>
                  <h4>Domicilio: {{ $postulante->domicilio }}</h4>
                  <h4>Teléfono: {{ $postulante->telefono }}</h4>
                  <h4>Fecha de expedición de título: {{ $postulante->fecha_titulo }}</h4>
                  <h4>Universidad otorgante: {{ $postulante->universidad_otorgante }}</h4>
                </div>
                <div class="panel-footer">
                  <h4>Antecedentes</h4>
                    <table class="table table-hover">
                      <thead>
                        <tr>
                            <td>Descripción</td>
                            <td>Institucion</td>
                            <td>Fecha de Emisión</td>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($postulante->antecedentes as $antecedente)
                        <tr>
                          <td>{{$antecedente->descripcion}}</td>
                          <td>{{$antecedente->institucion_otorgante}}</td>
                          <td>{{$antecedente->fecha_emision}}</td>
                        </tr>                    
                      @endforeach          
                      </tbody>
                    </table>
    
                  
                </div>
            </div>
        </div>
        <div class="col-md-4">
          <div class="panel">
              <div class="panel-header">
                <h3>Agregar antecedente</h3>
            </div>
            <div class="panel-body">
              <form method="POST" action="{{ route('antecedentes.store') }}">
                  {{ csrf_field() }}
                
                  <div class="form-group row">
                      <label for="descripcion" class="col-md-12 col-form-label text-md-left">{{ __('Descripción') }}</label>
                      <div class="col-md-12">
                          <input id="descripcion" type="text" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }}" name="descripcion" value="{{ old('descripcion') }}" required autofocus>

                          @if ($errors->has('descripcion'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('descripcion') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="fecha_emision" class="col-md-12 col-form-label text-md-left">{{ __('Fecha de emisión') }}</label>
                      <div class="col-md-12">
                          <input id="fecha_emision" type="date" class="form-control{{ $errors->has('fecha_emision') ? ' is-invalid' : '' }}" name="fecha_emision" value="{{ old('fecha_emision') }}" required autofocus>

                          @if ($errors->has('fecha_emision'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('fecha_emision') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="institucion_otorgante" class="col-md-12 col-form-label text-md-left">{{ __('Institución otorgante') }}</label>
                      <div class="col-md-12">
                          <input id="institucion_otorgante" type="text" class="form-control{{ $errors->has('institucion_otorgante') ? ' is-invalid' : '' }}" name="institucion_otorgante" value="{{ old('institucion_otorgante') }}" required autofocus>

                          @if ($errors->has('institucion_otorgante'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('institucion_otorgante') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>
                  &nbsp
                  <div class="form-group row mb-0">
                      <div class="col-md-12">
                          <button type="submit" class="btn btn-block btn-primary">
                              {{ __('Agregar antecedente') }}
                          </button>
                      </div>
                  </div>
                </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
