@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <table class="table table-striped">
            <thead class="thead-dark">
              <tr>
                <th>Nombre completo</th>
                <th>DNI</th>
                <th>Email</th>
                <th>Fecha de nacimiento</th>
                <th>Domicilio</th>
                <th>Teléfono</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              @foreach($postulantes as $postulante)
              <tr>
                <th>{{$postulante->apellido.', '.$postulante->nombre}}</th>
                <th>{{$postulante->dni}}</th>
                <th>{{$postulante->email}}</th>
                <th>{{$postulante->fecha_nac}}</th>
                <th>{{$postulante->domicilio}}</th>
                <th>{{$postulante->telefono}}</th>
                <th><a href="{{route('postulantes.show', $postulante->id)}}" class="btn btn-primary">Ver</a></th>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $postulantes->links() }}
        </div>
    </div>
</div>
@endsection
