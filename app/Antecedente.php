<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antecedente extends Model
{
	    // protected $table = 'antecedentes';

	 protected $fillable = [
        'descripcion',
        'fecha_emision',
        'institucion_otorgante',
        'user_id',
    ];

	 public function user()
	    {
	        return $this->belongsTo('App\User', 'user_id');
	    }
}
