<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Antecedente;
use App\User;
use Illuminate\Support\Facades\Auth;

class AntecedenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, array(
          'descripcion' => 'required|max:100',
          'fecha_emision' => 'required|date',
          'institucion_otorgante' => 'required|max:50',
        ));

        $antecedente = new Antecedente();
        $antecedente->descripcion = $request->descripcion;
        $antecedente->fecha_emision = $request->fecha_emision;
        $antecedente->institucion_otorgante = $request->institucion_otorgante;
        $antecedente->user_id = Auth::user()->id;
        $antecedente->save();
        return redirect(route('postulante.perfil'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
