<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'apellido',
        'nombre',
        'dni',
        'email',
        'password',
        'fecha_nac',
        'domicilio',
        'telefono',
        'fecha_titulo',
        'universidad_otorgante',
        'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'admin' => 'boolean',
    ];

    public function isAdmin()
    {
        return $this->admin;
    }

    public function antecedentes()
    {
        return $this->hasMany('App\Antecedente');
    }

    public function concursos()
    {
        return $this->belongsToMany('Concurso');
    }

    public function getNombre()
    {
        return $this->apellido.', '.$this->nombre;
    }
}
