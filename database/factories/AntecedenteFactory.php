<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Antecedente::class, function (Faker $faker) {
    return [
        	'descripcion' => $faker->randomElement(['Curso de Windows','Curso de Penal','Curso de Civil','Curso de Copy Paste','Curso de Word']),
        	'fecha_emision' => $faker->date(),
        	'institucion_otorgante' => $faker->randomElement(['UTN','UNT','UBA','UCA','UNSA','UNLP','UNC','UCLA']),
        	'user_id' => $faker->numberBetween(1,count(User::all())),
    ];
});


