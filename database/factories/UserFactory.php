<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
       'apellido' => $faker->lastName,
        'nombre' => $faker->name,
        'dni' => $faker->unique()->randomNumber($nbDigits = 8),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('1234'),
        'fecha_nac' => $faker->date($format = 'Y-m-d', $max = '1996-01-01'),
        'domicilio' => $faker->address,
        'telefono' => $faker->phoneNumber,
        'fecha_titulo' => $faker->date($format = 'Y-m-d'),
        'universidad_otorgante' => 'UNT',
    ];
});
