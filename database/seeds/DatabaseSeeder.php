<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Antecedente;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $maxUsuarios = 100;
        $maxAntecedentes = 300;
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        Antecedente::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        User::create([
            'apellido' => 'Galup',
            'nombre' => 'César',
            'dni' => '12345678',
            'email' => 'cesargalup@gmail.com',
            'password' => bcrypt('1234'),
            'fecha_nac' => '1981-06-23',
            'domicilio' => 'Las Heras',
            'telefono' => '333333333',
            'fecha_titulo' => '2019-03-18',
            'universidad_otorgante' => 'UTN - FRT',
            'admin' => true,
        ]);

        factory(User::class, $maxUsuarios)->create();

        factory(Antecedente::class, $maxAntecedentes)->create();

    }
}
